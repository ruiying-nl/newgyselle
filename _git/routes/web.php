<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/homepage');
});
Route::get('/portfolio', function () {
    return view('pages/portfolio');
});
Route::get('/achtergrond', function () {
    return view('pages/achtergrond');
});
Route::get('/team', function () {
    return view('pages/team');
});
Route::get('/contact', function () {
    return view('pages/contact');
});