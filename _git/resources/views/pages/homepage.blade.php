@extends('layout.mainlayout') @section ('content')
<div class="row my-5 home">
    <div class="col-12 col-lg-4 left">
        <!-- <div>
            <img class="w-100" src="/download/home_image_0.jpg" alt="" />
        </div> -->
        <div>
            <img class="w-100" alt="" src="/download/gysele.jpg" />
        </div>
        <p class="text  text-left my-5">
            Gysèle is vernoemd naar ballet Giselle: een wereldberoemd ballet uit
            de romantiek gecomponeerd door Adolphe Adam en gechoreografeerd door
            Jean Coralli en Jules Perrot, geïnspireerd door de legende van de
            Willi's uit De l'Allemagne. De première vond plaats in 1841 in de
            Parijse Opera. Naast bedrog en wraak voert pure liefde de boventoon.
            Als Giselle sterft aan een gebroken hart nadat ze ontdekt dat Graaf
            Albrecht al verloofd is, blijft Albrecht&nbsp;in radeloos verdriet
            achter.
        </p>
    </div>
    <div class="col-12 col-lg-8 px-lg-5 right">
        <div class="text text-left ">
            <p class="my-4">
                Met buro&nbsp;<strong>Gysèle Business &amp; Arts</strong> levert
                Gysèle ter Berg diensten als zakelijk leider, cultureel manager,
                artistic associate, fondsenwerver en creative producer. Met haar
                vaste team neemt zij gehele producties en processen uit
                handen.&nbsp;
            </p>
            <p class="my-4">
                Gysèle onderscheidt zich in het veld doordat zij iemand is die
                vanaf het begin van het creatieproces betrokken is bij het
                artistieke product van de maker. Iemand die strategieën en
                plannen kan formuleren en die bemiddelt tussen de maker, het
                publiek en partners om het werk de plaats in de markt te geven
                die het verdient. Zij is een ‘creative entrepreneur’ die
                volledig ingebed is in het creatieve proces met kennis van
                theater, dans en het culturele veld en gaat de dialoog aan met
                de maker/kunstenaar waarmee ze samenwerkt.&nbsp;Zo fungeert zij
                als spil aan de zijde van de artistiek leider, voert het
                financiële beheer en ontwikkelt beleid zonder de identiteit van
                een organisatie uit het oog te verliezen. Daarnaast zorgt zij
                steevast voor een gezonde zakelijke en financiële basis.
                Bovendien is Gysèle gespecialiseerd in het verkopen van
                dansvoorstellingen en is een zeer ervaren fondsenwerver. Tevens
                begeleidt en coacht zij jonge makers die na een vruchtbare tijd
                binnen een productiehuis hun vleugels willen uitslaan tot
                zelfstandig choreograaf.
            </p>
            <blockquote>
                <p class="my-4">
                    <em
                        >“De scherpte en snelheid waarop&nbsp;Gysèle&nbsp;zowel
                        inhoudelijk als zakelijk-strategisch functioneert, is
                        een enorme bijdrage aan mijn ontwikkeling als maker en
                        aan de groei van mijn company. Met haar out of the box
                        attitude ziet zij altijd kansen en legt verbindingen die
                        anderen niet zien.&nbsp;Gysèle&nbsp;is een geboren
                        manager en coach en geeft leiding aan publiciteit en
                        productie waarbij ze organisch richting bepaalt en
                        beslissingen neemt. Zij durft weloverwogen risico’s te
                        nemen. Ze </em
                    ><em
                        >geeft input tijdens audities, denkt mee over kostuums,
                        flyers en marketing acties.&nbsp;</em
                    ><em>Kortom, de stille kracht achter veel organisaties</em
                    >.”&nbsp;<strong
                        >Choreograaf Kalpana Raghuraman (Kalpanarts)</strong
                    >
                </p>
            </blockquote>
            <blockquote>
                <p class="my-4">
                    “Gysèle&nbsp;<em
                        >is een doorzetter pur sang, geeft nooit op. Ik noem
                        haar de hartklep van onze organisatie
                        BackBone.&nbsp;Gysèle&nbsp;biedt een vol pakket met
                        enorme ervaring in fondsenwerving en verkoop. Dat maakt
                        haar meer dan alleen een zakelijk leider. Zij kijkt
                        nauwkeurig naar de maker en naar het product. Ze moet je
                        voelen, zo ook het werk. Als die connectie er is krijgt
                        ze heel wat voor elkaar. &nbsp;Zo bedenkt ze
                        bijvoorbeeld artistieke concepten die makkelijk
                        inzetbaar zijn om voor de stichting extra geld te
                        creëeren. Dat maakt haar een allrounder en echte
                        creatieve ondernemer."&nbsp;</em
                    ><strong>Choreograaf Alida Dors (BackBone)</strong>
                </p>
            </blockquote>
            <blockquote>
                <p class="my-4">
                    "<em
                        >Gysèle denkt in oplossingen en in kansen. Ze is
                        duidelijk, to the point, helder, direct. Mijn werk
                        verkoopt zij al ruim 10 jaar lang aan Nederlandse
                        theaters. Het contact dat zij door de jaren heen met
                        programmeurs heeft opgebouwd is bijzonder. Er is sprake
                        van wederzijds vertrouwen in kwaliteit en
                        betrouwbaarheid. Tien jaar geleden heb ik daarnaast de
                        performance Happy Hour Chandelier
                        opgericht.&nbsp;Gysèle&nbsp;is onlangs eigenaar en
                        directeur geworden van deze BV. Ik zou niemand anders
                        liever willen zien die deze act vol liefde en aandacht
                        de wereld inbrengt. Zij doet al jaren het management van
                        <a href="http://www.happyhourchandelier.com"
                            >Happy Hour Chandelier</a
                        >, traint de performers en is verantwoordelijk voor het
                        floormanagent. Bij haar weet ik zeker dat Happy Hour
                        Chandelier in goede handen is</em
                    >.”<br />
                    <strong>Choreograaf Nanine Linning</strong>
                </p>
            </blockquote>
        </div>
    </div>
</div>

@endsection
