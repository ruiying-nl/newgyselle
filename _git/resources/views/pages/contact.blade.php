@extends('layout.mainlayout') @section ('content')
<div class="row my-5 flex-column text-center contact">
    <p>
        Gysèle Business &amp; Arts<br />
        Gysèle ter Berg<br />
        Hooivletstraat 50<br />
        1086 VH Amsterdam
    </p>
    <p>
        <a href="http://www.gysele.com">www.gysele.com</a><br />
        <a href="mailto:info@gysele.com" style="font-weight: bold;"
            >info@gysele.com</a
        ><br />
        +31 6 41502079
    </p>
    <p>
        KvK nummer: 51296276<br />
        BTW nummer: 127704644B01<br />
        Rabobank: NL65RABO0307957977
    </p>
</div>

</div>
@endsection
