@extends('layout.mainlayout') @section ('content')
<div class="row my-5">
    <div class="col-12 col-lg-4">
        <div class="list-group" id="list-tab" role="tablist">
            <img
                class="d-block w-100"
                src="http://www.gysele.com/uploads/development/projects_menu_image_0.jpg"
            />
            <a
                class="list-group-item list-group-item-action active odd"
                id="list-NanineLinning-list"
                data-toggle="list"
                href="#list-NanineLinning"
                role="tab"
                aria-controls="NanineLinning"
                >» Nanine Linning</a
            >
            <a
                class="list-group-item list-group-item-action even"
                id="list-HappyHourChandelier-list"
                data-toggle="list"
                href="#list-HappyHourChandelier"
                role="tab"
                aria-controls="HappyHourChandelier"
                >» Happy Hour Chandelier</a
            >
            <a
                class="list-group-item list-group-item-action odd"
                id="list-AmsterdamsKleinkunstFestival-list"
                data-toggle="list"
                href="#list-AmsterdamsKleinkunstFestival"
                role="tab"
                aria-controls="AmsterdamsKleinkunstFestival"
                >» Amsterdams Kleinkunst Festival</a
            >
            <a
                class="list-group-item list-group-item-action even"
                id="list-DeNederlandseDansdagen-list"
                data-toggle="list"
                href="#list-DeNederlandseDansdagen"
                role="tab"
                aria-controls="DeNederlandseDansdagen"
                >» De Nederlandse Dansdagen</a
            >
            <img
                src="http://www.gysele.com/uploads/development/projects_menu_image_1.jpg"
            />
        </div>
    </div>
    <div class="col-12 col-lg-8">
        <div class="tab-content" id="nav-tabContent">
            <div
                class="tab-pane fade show active"
                id="list-NanineLinning"
                role="tabpanel"
                aria-labelledby="list-NanineLinning-list"
            >
                <div class="right">
                    <section class="image">
                        <img
                            class="d-block w-100"
                            src="http://www.gysele.com/uploads/images/Nanine_header.jpg"
                        />
                    </section>
                    <section class="image_caption">
                        <p>
                            Fotografie Annemone Taake
                        </p>
                    </section>
                    <section class="text">
                        <p>
                            Gysèle is sinds 2007 nauw betrokken bij al het werk
                            van Nanine Linning in Nederland. Zij bekleedde
                            meerdere functies zoals zakelijk leider,
                            productieleider en PR &amp;
                            Marketing.&nbsp;Momenteel is haar core business het
                            verkopen van Nanine's&nbsp;voorstellingen. Ze
                            verkocht de voorstellingen Dolby, Endless Song of
                            Silence, Synthetic Twin, Voice Over, Zero,
                            Hieronymus B, Silver, Khora en Bacon aan Nederlandse
                            theaters.
                        </p>
                        <p>
                            <img
                                alt=""
                                src="http://www.gysele.com/uploads/images/NL_1.jpg"
                                style="width: 288px; height: 175px;"
                            />&nbsp;<img
                                alt=""
                                src="http://www.gysele.com/uploads/images/NL_2.jpg"
                                style="width: 288px; height: 175px;"
                            />
                        </p>

                        <p>
                            Nanine Linning maakt met haar producties
                            internationale furore. Zij onderscheidt zich door
                            haar multidisciplinaire handtekening waarin ze
                            verschillende kunstvormen laat versmelten tot een
                            nieuwe theatertaal. Voor haar voorstellingen werkt
                            ze samen met prominente kunstenaars uit de beeldende
                            kunst-, video-, design- en modewereld. Op het podium
                            waren in het verleden naast dansers ook acteurs,
                            dichters en musici&nbsp;aanwezig. Haar
                            voorstellingen onderscheiden zich zowel door
                            de&nbsp;bijzondere presentatievormen en locaties als
                            door haar theatrale events, die ze naast haar
                            voorstellingen presenteert als context voor haar
                            werk. Hiermee is Linning een choreografe die zich
                            graag buiten de bewandelde paden van het theater
                            begeeft.  Naast haar werk in Nederland
                            werkte&nbsp;Nanine in Duitsland als artistiek leider
                            van Dance Company Nanine Linning te Osnabrück en
                            daarna te Heidelberg (Duitsland).
                        </p>
                        <p>
                            <a href="http://www.naninelinning.nl"
                                >www.naninelinning.nl</a
                            ><br />
                            <a href="mailto:gysele@naninelinning.nl"
                                >gysele@naninelinning.nl</a
                            >
                        </p>
                        <p>
                            <img
                                alt=""
                                src="http://www.gysele.com/uploads/images/NL_3.jpg"
                                style="width: 288px; height: 425px;"
                            />&nbsp;<img
                                alt=""
                                src="http://www.gysele.com/uploads/images/NL_4.jpg"
                                style="width: 288px; height: 425px;"
                            /><br />
                            <span style="font-size: 10px;"
                                >Fotografie Kalle Kuikkaniemi &amp; Uwe
                                Lewandowski</span
                            >
                        </p>
                    </section>
                </div>
            </div>
            <div
                class="tab-pane fade"
                id="list-HappyHourChandelier"
                role="tabpanel"
                aria-labelledby="list-HappyHourChandelier-list"
            >
                <div class="right">
                    <section class="image">
                        <img
                            src="http://www.gysele.com/uploads/images/HHC_headerfoto.jpg"
                        />
                    </section>
                    <section class="image_caption">
                        <p>
                            Fotografie Jan Hendrix
                        </p>
                    </section>
                    <section class="text">
                        <p>
                            Gysèle is eigenaar en directeur van Happy Hour
                            Chandelier BV.
                        </p>
                        <p>
                            Happy Hour Chandelier is een uniek optreden bedacht
                            door choreografe Nanine Linning&nbsp;en designer
                            Marcel Wanders&nbsp;en wordt internationaal gezien
                            als hét nieuwe en exclusieve entertainment concept.
                            Happy Hour Chandelier is te zien op de meest
                            exclusieve feesten zoals o.a. Sex and The City
                            filmpremière London, de Cartier International Polo
                            te Londen, openingsfeest Nikki Beach te Marrakech,
                            opening Salone di Mobile te Milaan, 25
                            Most&nbsp;Influential Women Gala te Kiev, NYE te
                            Jumeirah Dubai, première Spice Girl Mel B’s reality
                            show te Hong Kong, etc.<br />
                            <br />
                            Het optreden duurt 20 minuten waarin een prachtige
                            danseres&nbsp;ondersteboven al dansend champagne
                            uitschenkt, omringd door een kroonluchter. Het
                            optreden is geschikt als openingsact, om te toasten
                            na een speech, voor zakelijke en privé feesten,
                            beurzen, openingen en lanceringen.
                        </p>
                        <p>
                            <a href="http://www.happyhourchandelier.com"
                                >www.happyhourchandelier.com</a
                            ><br />
                            <a
                                href="mailto:dancingangels@happyhourchandelier.com"
                                >dancingangels@happyhourchandelier.com</a
                            ><br />
                            <br />
                            <img
                                class="d-block"
                                alt=""
                                src="http://www.gysele.com/uploads/images/HHC_1.jpg"
                                style="width: 288px; height: 432px;"
                            />&nbsp;<img
                                class="d-block"
                                alt=""
                                src="http://www.gysele.com/uploads/images/HHC_2.jpg"
                                style="width: 288px; height: 432px;"
                            /><br />
                            <img
                                class="d-block"
                                alt=""
                                src="http://www.gysele.com/uploads/images/HHC_3.jpg"
                                style="width: 288px; height: 432px;"
                            />&nbsp;<img
                                class="d-block"
                                alt=""
                                src="http://www.gysele.com/uploads/images/HHC_4.jpg"
                                style="width: 288px; height: 432px;"
                            />
                        </p>

                        <p>
                            &nbsp;
                        </p>
                        <p>
                            &nbsp;
                        </p>
                    </section>
                </div>
            </div>
            <div
                class="tab-pane fade"
                id="list-AmsterdamsKleinkunstFestival"
                role="tabpanel"
                aria-labelledby="list-AmsterdamsKleinkunstFestival-list"
            >
                ...
            </div>
            <div
                class="tab-pane fade"
                id="list-DeNederlandseDansdagen"
                role="tabpanel"
                aria-labelledby="list-DeNederlandseDansdagen-list"
            >
                ...
            </div>
        </div>
    </div>
</div>
@endsection
