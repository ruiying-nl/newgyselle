@extends('layout.mainlayout') @section ('content')
<div class="row my-5 achtergrond">
    <div class="col-12 col-lg-4 left">
        <div
            class="achtergrond-img w-100 d-flex flex-column justify-content-end"
        >
            <div class="w-100 mb-4 text ">
                <img
                    class="d-block w-75 mx-auto"
                    alt=""
                    src="http://www.gysele.com/uploads/images/IMG_5368.JPG"
                />
                <p class="w-75 mx-auto my-3">
                    <span
                        >Gysele en choreograaf Kalpana Raghuraman
                        &nbsp;&nbsp;</span
                    ><span>Fotografie Bowie Verschuuren</span>
                </p>
                <img
                    class="d-block w-75 mx-auto"
                    alt=""
                    src="http://www.gysele.com/uploads/files/Alied-en-Gys.jpg"
                />
            </div>
        </div>
        <p class="text px-md-3 text-center my-3">
            <span>Gysele en choreograaf Alida Dors &nbsp;&nbsp;</span
            ><span>Fotografie Loes Schakenbos</span>
        </p>
    </div>
    <div class="col-12 col-lg-8 px-lg-5 right">
        <div class="text text-left ">
            <p class="my-4">
                Gysèle studeerde Theaterwetenschap en Kunstbeleid &amp;
                Management aan de Universiteit van Utrecht, volgde de
                vooropleiding van de toneelacademie te Maastricht, en
                de&nbsp;acteursopleiding ‘Theaterhuis De Trap’ te Amsterdam.
                Inmiddels heeft zij 13 jaar deskundige ervaring in het culturele
                veld.
            </p>
            <h3 class="my-4">
                <span style="color:#ff0000;">“Als professional...</span>
            </h3>
            <p class="my-4">
                ... heb ik een voorkeur voor het werken in een resultaatgerichte
                omgeving waarbij een ‘product’ van A tot Z ontstaat. Ik heb een
                ondernemersgeest en ben daarnaast een teamplayer. Ik heb oog
                voor detail en 'tone of voice'. Geregeld ben ik in de repetitie
                studio te vinden; betrokkenheid met maker en praktijk zijn mijn
                inziens voorwaarden voor een gezonde bedrijfsvoering en
                daarnaast mijn ‘voeding’ voor het werk. Mijn kracht ligt in het
                hebben van een helikopterview en een juiste balans tussen een
                zakelijke en persoonlijke benadering. Daarbij levert een
                praktijkervaring als dramaturg een fundamentele bijdrage aan
                mijn zakelijk inzicht. Juist door deze praktijkervaring streef
                ik voortdurend naar een optimale integratie van artistieke en
                zakelijke kwaliteiten, waarbij communicatie een van de
                belangrijkste spelers is. Loyaliteit, inspiratie, transparantie,
                authenticiteit en humor zijn belangrijke waarden in mijn werk."
            </p>
            <h3 class="my-4">
                <span style="color:#ff0000;">“Als privé persoon... </span>
            </h3>
            <p class="my-4">
                ... ben ik Bourgondisch te noemen en heb ik een positieve en
                optimistische kijk op het leven. Ik ben betrokken, eerlijk en
                enthousiast. Ik hou van een goed gesprek bij een glas wijn, maar
                kan ook in stilte genieten. Ik ben altijd op zoek naar balans.
                Ik probeer maximaal te leven zonder mij te forceren. Reizen en
                koken zijn onlosmakelijk met mij verbonden. Ik ontdek graag de
                wereld, en de wereld ontdekt mij."
            </p>
            <h3 class="my-4">
                <span style="color:#ff0000;">“Tijdens mijn studietijd... </span>
            </h3>
            <p class="my-4">
                ... speelde ik in diverse theatervoorstellingen, acteerde ik
                voor televisie, regisseerde ik voorstellingen, en was ik als
                dramaturg betrokken bij diverse producties. Ik gaf
                auditie-coaching en schreef theaterteksten. Daarnaast werkte ik
                bij Stadschouwburg Amsterdam. In dit culturele hart van de stad
                heb ik veel tijd doorgebracht. Je leert en leeft door te doen.
                Met intuïtie, goed verstand en een dosis lef. Ik spring vaak met
                enthousiasme het diepe in, hoe spannend ook.
            </p>
            <p>
                &nbsp;
            </p>
        </div>
    </div>
</div>

@endsection
