@extends('layout.mainlayout') @section ('content')
<div class="row my-5 achtergrond">
    <div class="col-12 col-lg-4 left">
        <div
            class="achtergrond-img w-100 d-flex flex-column justify-content-end"
        >
            <div class="w-100 m-3 text ">
                <img
                    class="d-block w-50 "
                    alt=""
                    src="http://www.gysele.com/uploads/images/Parsifall-1_24_09_18.jpg"
                />
                <p class="w-50 ">
                    Fotografie Maarten Dikken
                </p>
                <img
                    class="d-block w-50"
                    alt=""
                    src="http://www.gysele.com/uploads/images/_MG_3628.jpg"
                />
                <p class="w-50 ">
                    Fotografie Simone Giacomini
                </p>
                <img
                    class="d-block w-50"
                    alt=""
                    src="http://www.gysele.com/uploads/images/_MG_3669.jpg"
                />
                <p class="w-50 ">
                    Fotografie Simone Giacomini
                </p>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-8 px-lg-5 right">
        <div class="text text-left ">
            <p class="my-4">
                Gysèle werkt met een vast team van freelancers om alle aspecten
                van productie, publiciteit, fotografie, grafisch ontwerp en
                software ontwikkeling op te vangen.
            </p>
            <p class="my-4">
                Buro Gysèle Business &amp; Arts bestaat uit:
            </p>
            <p class="my-4">
                <em>Gysèle ter Berg</em>, directeur en zakelijk leider
            </p>
            <p class="my-4"><em>Susanne Tuny</em>, productieleiding&nbsp;</p>
            <p class="my-4">
                <em>Nanda van Aalst</em>, PR &amp; Marketing&nbsp;
            </p>
            <p class="my-4">
                <em>Jeffrey Steenbergen</em>, technische productie &amp;
                lichtontwerper
            </p>
            <p class="my-4"><em>Serena Kloet</em>, grafisch ontwerp&nbsp;</p>
            <p class="my-4"><em>Kunst in Zaken</em>, administratiekantoor</p>
            <p class="my-4">
                Daarnaast is zij partnerships aangegaan met Bowie Verschuuren
                (trailers, video's, scenefotografie), Leo van Emden
                (registraties) en Jean van Lingen (scenefotografie).
            </p>
            <p>
                &nbsp;
            </p>
        </div>
    </div>
</div>

@endsection
