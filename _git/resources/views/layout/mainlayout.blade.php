<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
            integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
            crossorigin="anonymous"
        />
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
            crossorigin="anonymous"
        />
        <link rel="stylesheet" href="/css/style.css" />
        <script type="text/javascript">
            var csrf_token = "{{ csrf_token() }}";
        </script>

        <title>Gysele</title>
    </head>
    <body>
        <div class="container">
            <a id="top"></a>
            <header class="banner my-4 mx-auto">
                <a
                    id="header"
                    href="/"
                    target="_self"
                    title="Gysèle Business &amp; Arts"
                >
                    <img
                        src="/download/header.png"
                        alt="Gysèle Business &amp; Arts"
                        class="w-100"
                    />
                </a>
            </header>

            <nav class="navbar  navbar-expand-lg py-0 mx-auto">
                <button
                    class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div
                    class="collapse navbar-collapse justify-content-center py-0 "
                    id="navbarSupportedContent"
                >
                    <ul class="d-flex justify-content-between w-100 py-0">
                        <li class="nav-item home-link">
                            <a class="nav-link active" href="/">home</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/achtergrond"
                                >achtergrond</a
                            >
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/portfolio">portfolio</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/contact">contact</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/team">team</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <main>
                @yield('content')
            </main>
            <footer class="mx-auto container p-3">
                <div class="row flex-nowrap">
                    <div
                        class="col-12 col-lg-6 ml-3 d-flex flex-column justify-content-end"
                    >
                        <p class="pb-0 mb-0">
                            Gysèle ter Berg |

                            <a
                                class="text-dark"
                                href="mailto:info@gysele.com"
                                title="info@gysele.com"
                                >info@gysele.com</a
                            >
                            | +31 6 41502079
                        </p>
                        <p class="">
                            Gysèle Business &amp; Arts @2017
                        </p>
                    </div>
                    <div class="col-12 col-lg-3">
                        <img src="/download/footer_visual.png" alt="" />
                    </div>
                    <div class="col-12 col-lg-3 text-right pr-5">
                        <p class="">
                            <img src="/download/btn_facebook.png" alt="" /><img
                                src="/download/btn_twitter.png"
                                alt=""
                            />
                        </p>
                        <ul class="mt-5">
                            <li class="">
                                <a class="" href="/">home</a>
                            </li>

                            <li class="">
                                <a class="" href="/achtergrond">Achtergrond</a>
                            </li>

                            <li class="">
                                <a class="" href="/portfolio">Portfolio</a>
                            </li>

                            <li class="">
                                <a class="" href="/contact">Contact</a>
                            </li>

                            <li class="">
                                <a class="" href="/partners">Partners</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
        <script
            src="http://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"
        ></script>
        <!-- <script type="text/javascript" src="/js/app.js"></script> -->
    </body>
</html>
